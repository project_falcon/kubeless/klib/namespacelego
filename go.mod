module gitlab.com/project_falcon/kubeless/klib/namespacelego

go 1.14

require (
	gitlab.com/project_falcon/kubeless/klib/toolslego v0.24.11
	gitlab.com/project_falcon/kubeless/lib/payload v1.52.1
	k8s.io/api v0.19.4
	k8s.io/apimachinery v0.19.4
)
