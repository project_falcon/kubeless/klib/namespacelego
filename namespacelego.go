package namespacelego

import (
	"gitlab.com/project_falcon/kubeless/klib/toolslego"
	"gitlab.com/project_falcon/kubeless/lib/payload"
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func GetDefinition(apiEvent *payload.APIData) (*v1.Namespace, error) {

	clusterName, err := toolslego.GetNamespace(apiEvent)
	if err != nil {
		return nil, err
	}

	nsDefinition := v1.Namespace{
		ObjectMeta: metaV1.ObjectMeta{
			Name:   clusterName,
			Labels: toolslego.GetClusterLabels(apiEvent),
		},
	}

	return &nsDefinition, err
}

func GetUpdateDefinition(ns payload.Namespace) (*v1.Namespace){


	nsDefinition := v1.Namespace{
		ObjectMeta: metaV1.ObjectMeta{
			Name:   ns.Name,
			Labels: ns.LabelsMap,
		},
	}

	return &nsDefinition
}